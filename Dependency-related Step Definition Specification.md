# 依赖相关 Step 定义规范

## 1. archive_extract 与 cab_extract

archive_extract 和 cab_extract 的作用都是下载并解压缩文件到指定目录，但是之所以要用两个 action 表示是因为 cab 压缩包可能是 exe 自解压格式封装的，在自动类型识别时不太容易区分是正常 exe 程序还是 cab 压缩包格式，故此增加 cab_extract 专指 exe 封装格式的压缩包，降低编程复杂度。
archive_extract 支持的压缩包格式包括： "zip", "rar", "7z", "cab", "xz", "gz"

这两个 action 的格式如下:

```
- action: archive_extract  # 或者cab_extract
  file_name: d3dx9.tar.xz  # 暂未使用
  url: https://proxy.usebottles.com/redistributable/dependencies/d3dx9.tar.xz     # 下载链接
  file_checksum: 0e444b86310e732f50a971466f3f06a3  #暂未使用
  file_size: 19651992				# 暂未使用
  dest: temp/d3dx9/					# 解压的目标路径。均使用相对路径
```

## 2. install_fonts

将一种字体文件拷贝到系统目录中。

action 格式示例如下：

```
- action: install_fonts
  url: temp/consolas-fonts-tts.tar/ # archive_extract动作解压后的文件路径
  fonts:             				# url指定的目录下字体文件列表
  - Consolas-Bold.ttf
```

## 3. register_font

将一种字体文件注册到系统注册表中。
action 格式示例如下：

```
- action: register_font
  name: Source Han Sans K Bold          # 字体名
  file: SourceHanSans.ttc               # 文件名
```

## 4. replace_font

Replacement Font，将一种字体替换为另一种字体。
注册表位置:HKEY_CURRENT_USER/Software/Wine/Fonts/Replacement

action 格式示例如下：

```
- action: replace_font
  font: Source Han Sans TC
  replace:
  - DFKai-SB
  - Microsoft JhengHei
  - Microsoft JhengHei UI
```

上面的例子意思是将 replace 数组中的字体替换为 font 指定的数组。

## 5. copy_file

将下载、解压后的文件从一个地方拷贝到另一个地方。

action 格式示例如下：

```
- action: copy_file
  file_name: '*.dll'					# 文件名可以包含通配符 *
  url: temp/W2KSP4_EN/i386/		# 下载、解压后的文件路径
  dest: SXEarth4.2.0/bin_32		#	目标路径，使用C盘下的相对路径。实际路径c:/SXEarth4.2.0/bin_32
```

## 6. override_dll

指定如何处理特定的 DLL，native 或 n 使用 Microsoft Windows 的.DLL 文件。builtin 或 b 使用 wine 自己实现的“内置的”DLL 。

对应注册表位置：HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides

action 支持两种格式，示例如下：

```
# 格式一：
- action: override_dll
  dll: riched20							# dll 文件名称
  type: native,builtin			# native优先。可是是 n,b或b,n或b或n

# 格式二：
- action: override_dll
  bundle:
    - value: odbccp32				# dll 文件名称
      data: native,builtin	# native优先。可是是 n,b或b,n或b或n
    - value: mtxdm
      data: native,builtin
    - value: odbc32
      data: native,builtin
    - value: oledb32
      data: native,builtin
```

## 7. set_register_key

设置特定注册表项。

action 格式示例如下：

```
- action: set_register_key
  key: HKLM\\Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full
  value: Install
  data: '0001'
  type: REG_DWORD
```

## 8. set_windows

配置 wine 容器的 windows 版本。

action 格式示例如下：

```
- action: set_windows
  version: win7
```

supported versions:

- win10 (Microsoft Windows 10)
- win81 (Microsoft Windows 8.1)
- win8 (Microsoft Windows 8)
- win7 (Microsoft Windows 7)
- win2008r2 (Microsoft Windows 2008 R1)
- win2008 (Microsoft Windows 2008)
- winxp (Microsoft Windows XP)

## 9. install

安装 exe 或 msi 类型可执行程序文件。

action 格式示例如下：

```
- action: install_exe
  environment:														# 安装时的环境变量
    WINEDLLOVERRIDES: fusion=b
  file_name: dotNetFx40_Full_x86_x64.exe # 如果是压缩包文件此处要填写解压后要执行的exe文件路径，如果有多级目录要包含上层目录
  url: http://download.microsoft.com/download/9/5/A/95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE/dotNetFx40_Full_x86_x64.exe						# 下载链接
  arguments: /q													# 安装参数，/q 静默安装
  file_checksum: 251743dfd3fda414570524bac9e55381	#暂未使用
  file_size: 50449456										#暂未使用
```

## 10. register_dll

在 windows 上注册 OLE 控件（DLL 或 ActiveX ）。activex 不注册是不能够被系统识别和使用的。例如 jet40.dll、mstscax.dll 和 mdac28.dll 就是需要注册才能使用。

**暂未实现**

action 格式示例如下：

```
- action: register_dll
  dll: mstscax							# dll 文件名称
```

## 11. uninstall

软件卸载。

**暂未实现**

action 格式示例如下：

```
- action: uninstall
  file_name: Wine Mono
```
